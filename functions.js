function loger(val1, val2) {
    var obj = {
        values: {
            val1: val1,
            val2: String(val2)
        },
        typesBefore: {
            val1: (typeof val1),
            val2: (typeof val2)
        },
        results: {
            plus: (val1 + val2),
            minus: (val1 - val2)
        },
        typesAfter: {

        }
    };

    obj.typesAfter.plus = typeof obj.results.plus;
    obj.typesAfter.minus = typeof obj.results.minus;

    foreachObj(obj);
    console.log('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&');

    function foreachObj(obj, name) { //в ф-цию передаем массив
        if(name !== undefined){
            console.log('-----------' + name + '-------------');
        }
        for(key in obj){
            if(obj.hasOwnProperty(key)){
                if( obj[key] === Object(obj[key])){
                    foreachObj(obj[key], key);
                }
                else {
                    console.log(key + ": " + obj[key])
                }
            }
        }
    }
}

